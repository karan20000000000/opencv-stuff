
import cv2
import urllib2
import numpy as np

hoststr = 'http://192.168.0.3:8080/video'

stream=urllib2.urlopen(hoststr)

byt=''
while True:
    byt+=stream.read(1024)

    """
    ...(http)
    0xff 0xd8      --|
    [jpeg data]      |--this part is extracted and decoded
    0xff 0xd9      --|
    ...(http)
    0xff 0xd8      --|
    [jpeg data]      |--this part is extracted and decoded
    0xff 0xd9      --|
    ...(http)
    """

    a = byt.find('\xff\xd8')
    b = byt.find('\xff\xd9')
    if a!=-1 and b!=-1:
        jpg = byt[a:b+2]
        byt= byt[b+2:]
        i = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.IMREAD_COLOR)
        i = cv2.resize(i,(640,480))
        cv2.imshow(hoststr,i)
        if cv2.waitKey(1) ==27:
            break
