import cv2
import numpy as np
import urllib
import cv2.cv2 as cv

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    if ret:
        #Convert BGR to HSV
        hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

        #Define range of desired color in HSV
        # # lower_orange = np.array([0,144,215])
        # lower_orange = np.array([10,80,50])
        lower_orange = np.array([0,112,94])
        upper_orange = np.array([28,255,255])

        # lower_orange = np.array([0,144,215])
        # upper_orange = np.array([34,255,255])


        #Threshold the HSV image to get desired color
        mask = cv2.inRange(hsv, lower_orange, upper_orange)
        #Bitwise-AND mask and original image
        res = cv2.bitwise_and(frame,frame, mask= mask)
        #Morphological operations
        kernel = np.ones((5,5),np.uint8)
        erosion = cv2.erode(mask,kernel,iterations = 1)
        dilation = cv2.dilate(mask,kernel,iterations = 1)
        opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
        closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        # Detect circles using HoughCircles
        circles = cv2.HoughCircles(closing,cv.HOUGH_GRADIENT,2,120,param1=100,param2=200,minRadius=0,maxRadius=0)

        #Draw Circles
        if circles is not None:
            for i in circles[0,:]:
                # If the ball is far, draw it in green
                    cv2.circle(frame,(int(round(i[0])),int(round(i[1]))),int(round(i[2])),(255,0,0),4)
                    cv2.circle(frame,(int(round(i[0])),int(round(i[1]))),1,(0,255,0),4)

        cv2.imshow('Closing',closing)
        cv2.imshow('tracking',frame)
        # cv2.imshow('Original',frame)
        cv2.imshow('Mask',mask)
        cv2.imshow('Result',res)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
