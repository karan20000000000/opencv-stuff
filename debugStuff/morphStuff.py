import urllib
import cv2
import numpy as np
import time

url='http://192.168.0.5:8080/shot.jpg'

while True:

	# Use urllib to get the image and convert into a cv2 usable format
    imgResp=urllib.urlopen(url)
    imgNp=np.array(bytearray(imgResp.read()),dtype=np.uint8)
    frame=cv2.imdecode(imgNp,-1)

    # frame = cv2.resize(img,(640,480))
	# put the image on screen
    # cv2.imshow('IPWebcam',frame)

    #Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #Define range of desired color in HSV
    # lower_red = np.array([30,150,50])
    # upper_red = np.array([255,255,180])
    lower_orange = np.array([0,144,215])
    upper_orange = np.array([34,255,255])

    #Threshold the HSV image to get desired color
    mask = cv2.inRange(hsv, lower_orange, upper_orange)
    #Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= mask)

    #Morphological operations
    kernel = np.ones((5,5),np.uint8)
    erosion = cv2.erode(mask,kernel,iterations = 1)
    dilation = cv2.dilate(mask,kernel,iterations = 1)
    opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    cv2.imshow('Erosion',erosion)
    cv2.imshow('dilation',dilation)
    cv2.imshow('Original',frame)
    cv2.imshow('Mask',mask)
    cv2.imshow('Opening',opening)
    cv2.imshow('Closing',closing)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
                break


cv2.destroyAllWindows()
